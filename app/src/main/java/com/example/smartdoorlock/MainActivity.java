package com.example.smartdoorlock;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    final String TAG = "Main Activity ------- ";
    String pw_entered = "";
    String deviceHardwareAddress; //bluetooth mac address of the paired device

    final static int REQUEST_ENABLE_BT = 1;
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;
    FirebaseFirestore db_users;
    FirebaseFirestore db_unlock;
    ArrayList<UserInfo> userList = new ArrayList<UserInfo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db_users = FirebaseFirestore.getInstance();
        db_unlock = FirebaseFirestore.getInstance();


        if(bluetoothAdapter == null) {
            Log.i(TAG, "Device does not support bluetooth!!");
        }

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            Log.i(TAG, "BT is not enabled");
        }

        new ConnectThread().start();

        Intent login_back_intent = getIntent();
        Intent admin_back_intent = getIntent();
        Intent permission_back_intent = getIntent();
    }

    public void enter(View view) {
        TextView mText = (TextView) findViewById(R.id.textView);
        db_users.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                String deviceID = document.getData().get("id").toString();
                                String passcode = document.getData().get("passcode").toString();
                                String userName = document.getData().get("name").toString();

                                Log.i(TAG, "~~~~~~~pw_entered = " + pw_entered + ", passcode = "+ passcode);

                                if (deviceID.equals(deviceHardwareAddress) && passcode.equals(pw_entered)){
                                    mText.setText("DOOR UNLOCK");
                                    sendSignalToMCU("1");

                                    Toast.makeText(MainActivity.this, "Door unlocked", Toast.LENGTH_SHORT).show();

                                    Date currentTime = Calendar.getInstance().getTime();
                                    Log.i(TAG, "~~~~~~~~-----~~~~["+currentTime.toString()+"~~~~~]");
                                    addEventToDb(deviceHardwareAddress, userName, currentTime.toString());

                                } else {
                                    Log.w(TAG, "Error getting documents.", task.getException());
                                }
                            }
                            Toast.makeText(MainActivity.this, "INCORRECT PASSCODE", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    public void clear(View view) {
        TextView mText = (TextView) findViewById(R.id.textView);
        mText.setText("Door locked");
        sendSignalToMCU("0");
        pw_entered = "";
    }

    public void adminLogIn(View view) {
        Intent adminIntent = new Intent(this, LoginActivity.class);
        startActivity(adminIntent);
    }

    public void btn1(View view) {
        pw_entered = pw_entered + '1';
    }
    public void btn2(View view) {
        pw_entered = pw_entered + '2';
    }
    public void btn3(View view) {
        pw_entered = pw_entered + '3';
    }
    public void btn4(View view) {
        pw_entered = pw_entered + '4';
    }
    public void btn5(View view) {
        pw_entered = pw_entered + '5';
    }
    public void btn6(View view) {
        pw_entered = pw_entered + '6';
    }
    public void btn7(View view) {
        pw_entered = pw_entered + '7';
    }
    public void btn8(View view) {
        pw_entered = pw_entered + '8';
    }
    public void btn9(View view) {
        pw_entered = pw_entered + '9';
    }
    public void btn0(View view) {
        pw_entered = pw_entered + '0';
    }

    private void sendSignalToMCU(String msgToMCU) {
        Log.i(TAG, "called send signal to mcu");
        if(mmSocket != null) {
            try {
                Log.i(TAG, "sending");
                mmSocket.getOutputStream().write(msgToMCU.toString().getBytes());
            } catch (IOException e) {
                Log.e(TAG, "sendSignalToMCU() error");
            }
        }
    }

    private class ConnectThread extends Thread {

        public ConnectThread() {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            try {
                Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

                if (pairedDevices.size() > 0) {
                    // There are paired devices. Get the name and address of each paired device.
                    for (BluetoothDevice device : pairedDevices) {
                        String deviceName = device.getName();
                        deviceHardwareAddress = device.getAddress(); // MAC address
                    }
                }

                mmDevice = bluetoothAdapter.getRemoteDevice("98:D3:B1:FD:BD:4C");


                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                mmSocket = mmDevice.createRfcommSocketToServiceRecord(MY_UUID);

            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            bluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }

    public void addEventToDb(String BTAddress, String name, String dateTime){
        HashMap<String, String> unlock = new HashMap<>();
        unlock.put("addr", BTAddress);
        unlock.put("username", name);
        unlock.put("accesstime", dateTime);
        Log.i(TAG, "~~~~~~~line 234");
        db_unlock.collection("unlock").add(unlock).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Log.i(TAG, "unlock Document added with time: " + dateTime);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i(TAG, "Error adding Doc unlock");
            }
        });
    }
}