package com.example.smartdoorlock;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class AdminActivity extends AppCompatActivity implements OnCompleteListener<QuerySnapshot> {
    final String TAG = "Admin Activity-------";
    ArrayList<String> logs = new ArrayList<>();
    FirebaseFirestore db_unlock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        Intent intent_admin_loggedin = getIntent();

        db_unlock = FirebaseFirestore.getInstance();

        db_unlock.collection("unlock")
                .get()
                .addOnCompleteListener(this);
    }

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        ListView listView = (ListView) findViewById(R.id.listView_access);
        Log.i(TAG, ">>>>>>>>>>>>>>line 41");
        if (task.isSuccessful()) {
            QuerySnapshot qs = task.getResult();

            Log.i(TAG, ">>>>>>>>>>>>>>line 43: "+qs);
            for (QueryDocumentSnapshot document : qs) {
                Log.i(TAG, ">>>>>>>>>>>>>>line 999");
                String log = document.getData().get("username").toString() + "\n" + document.getData().get("accesstime").toString();
                logs.add(log);
                Log.i(TAG, ">>>>>>>>>>>>>>["+log+"]");
            }
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, logs);
            listView.setAdapter(arrayAdapter);
        }

    }

    public void back_mainpage(View view) {
        Intent back_Intent = new Intent(this, MainActivity.class);
        startActivity(back_Intent);
    }

    public void goToGivePermission(View view) {
        Intent givePermission_Intent = new Intent(this, PermissionActivity.class);
        startActivity(givePermission_Intent);
    }


}