package com.example.smartdoorlock;

public class UserInfo {
    private String name;
    private String deviceId;
    private String passcode;

    UserInfo(String name, String deviceId, String passcode) {
        this.name = name;
        this.deviceId = deviceId;
        this.passcode = passcode;
    }

    UserInfo(){
        ;
    }

    String getName() {
        return this.name;
    }

    String getDeviceId(){
        return this.deviceId;
    }

    String getPasscode(){
        return this.passcode;
    }

    void setName(String name) {
        this.name = name;
    }

    void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    void setPasscode(String passcode) {
        this.passcode = passcode;
    }
}
