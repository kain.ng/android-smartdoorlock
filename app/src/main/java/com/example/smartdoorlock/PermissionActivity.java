package com.example.smartdoorlock;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;

public class PermissionActivity extends AppCompatActivity {
    final String TAG = "Permission Acty ------ ";
    //ArrayList<UserInfo> userList = new ArrayList<UserInfo>();
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        Intent intent_give_permission = getIntent();

        db = FirebaseFirestore.getInstance();
    }


    public void logout_from_permission(View view) {
        Intent back_Intent_permission = new Intent(this, MainActivity.class);
        startActivity(back_Intent_permission);
    }

    public void addUser(View view) {
        UserInfo authorizedPerson = new UserInfo();
        HashMap<String, String> user = new HashMap<>();

        EditText mAddDeviceId = (EditText) findViewById(R.id.editText_deviceID);
        String deviceId = mAddDeviceId.getText().toString();

        EditText mAddName = (EditText) findViewById(R.id.editText_name);
        String name = mAddName.getText().toString();

        EditText mPasscode = (EditText) findViewById(R.id.editText_passcode);
        String passcode = mPasscode.getText().toString();

        Log.i(TAG, "["+deviceId+"], ["+name+"], ["+passcode+"]");

        if (deviceId.isEmpty() || name.isEmpty() || passcode.isEmpty()) {
            Toast.makeText(PermissionActivity.this, "Invalid input", Toast.LENGTH_SHORT);

        } else {
            Toast.makeText(PermissionActivity.this, "User Added", Toast.LENGTH_SHORT);
            user.put("name", name);
            user.put("id", deviceId);
            user.put("passcode", passcode);

            db.collection("users").add(user).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                @Override
                public void onSuccess(DocumentReference documentReference) {
                    Log.i(TAG, "Document added with ID "+documentReference.getId());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.i(TAG, "Error adding Doc");
                }
            });

        }

    }
}