package com.example.smartdoorlock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {
    private final String FACTORY_PASSWORD = "9876";
    EditText passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Intent intent = getIntent();
    }

    public void back(View view) {
        Intent backIntent = new Intent(this, MainActivity.class);
        startActivity(backIntent);
    }

    public void login(View view) {
        passwordText = (EditText)findViewById(R.id.editText_password);
        Editable input_pw = passwordText.getText();
        Log.e("--------------", "["+input_pw.toString()+"]");
        if (input_pw.toString().equals(FACTORY_PASSWORD) ) {
            Intent loginIntent = new Intent(this, AdminActivity.class);
            startActivity(loginIntent);
        } else {
            Toast.makeText(LoginActivity.this, R.string.incorrect_password_toast, Toast.LENGTH_SHORT).show();
        }
    }


}